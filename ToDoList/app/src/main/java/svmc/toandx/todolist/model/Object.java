package svmc.toandx.todolist.model;

public class Object {
    String title ;

    public Object(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}