package svmc.toandx.todolist.notification;

        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.widget.Toast;

        import androidx.core.app.NotificationManagerCompat;



/*
        Tao button Completed trong Notifcation
 */
public class CompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = Integer.parseInt( intent.getStringExtra("ID")  );
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context.getApplicationContext());
        notificationManagerCompat.cancel(id);
        Toast.makeText(context, "The task is completed" , Toast.LENGTH_SHORT).show();
    }
}
