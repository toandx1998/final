package svmc.toandx.todolist.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import svmc.toandx.todolist.model.Task;
import svmc.toandx.todolist.R;

public class TaskAdapter extends BaseAdapter {
    ArrayList<Task> arr ;
    Context context ;
    Calendar calendar=Calendar.getInstance();

    public interface  OnClickCheckBox{
        void onClickCheckBox(Task task);
    }
    public interface  OnClickTextView{
        void onClickTextView(Task task);
    }
    OnClickTextView onClickTextView;
    OnClickCheckBox onClickCheckBox;

    public TaskAdapter(ArrayList<Task> arr, Context context, OnClickCheckBox onClickCheckBox,OnClickTextView onClickTextView) {
        this.arr = arr;
        this.context = context;
        this.onClickCheckBox = onClickCheckBox;
        this.onClickTextView = onClickTextView;
    }

    @Override
    public int getCount() {
        return arr.size();
    }

    @Override
    public Object getItem(int i) {
        return arr.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class ViewHolder{
        TextView textViewTitle;
        TextView textViewDueTime;
        CheckBox checkBox;
    }
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder ;
        if(view == null){
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.item_listview,viewGroup,false);
            holder.checkBox = view.findViewById(R.id.cb_status);
            holder.textViewTitle = view.findViewById(R.id.tv_title);
            holder.textViewDueTime = view.findViewById(R.id.tv_due_time);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickCheckBox.onClickCheckBox(arr.get(i));
            }
        });
        holder.textViewTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickTextView.onClickTextView(arr.get(i));
            }
        });
        holder.textViewDueTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickTextView.onClickTextView(arr.get(i));
            }
        });
        holder.textViewTitle.setText(arr.get(i).title);
        if (arr.get(i).status==1)
        {
            holder.textViewTitle.setTextColor(Color.parseColor("#000000"));
            holder.checkBox.setChecked(true);
            holder.textViewTitle.setPaintFlags(holder.textViewTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else {
            holder.textViewTitle.setTextColor(Color.parseColor("#00FF22"));
            holder.checkBox.setChecked(false);
            holder.textViewTitle.setPaintFlags(holder.textViewTitle.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
        calendar.setTimeInMillis(arr.get(i).dueTime);
        SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        holder.textViewDueTime.setText(sdf.format(calendar.getTime()));
        return view;
    }
}